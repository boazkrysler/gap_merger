import glob
import logging
import ConfigParser

import errno

from datetime import datetime

import subprocess

import sys

from gap_BB_printer import read_txt_file
from csv_handler import create_report_folder, create_merger_file, get_all_id_from_merger_file
from path_merger import find_similar_paths, find_number_of_segments

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
logging_level = config.get('logging', 'logging_level')
logger_name = config.get('logging', 'name')

path_save_files =  config.get('paths', 'path_turkic_txt_files')


def run(read_path, date_of_exec):
    start_time = datetime.now()

    read_path += "/"
    files = glob.glob(read_path + "*.txt")
    for name in files:  # 'file' is a builtin type, 'name' is a less-ambiguous variable name.
        try:
            file_start_time = datetime.now()
            txt_lines, video_name, track_list = read_txt_file(name)
            number_of_segments = find_number_of_segments(video_name)
            if number_of_segments > 1:

                logging.info("working on file: %s with %s segments" %(video_name, number_of_segments))
                print "working on file: %s with %s segments" %(video_name, number_of_segments)

                ##merger_file
                merged_list = []

                create_report_folder(video_name)
                path_person, path_other = find_similar_paths(txt_lines, video_name, track_list, date_of_exec, merged_list)

                runningTime = str(datetime.now() - file_start_time)
                logging.info("finished file in: %s seconds", runningTime)

                try:        #delete temp files
                    cmdLine = "sudo rm -rf " + path_person
                    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
                    cmdLine = "sudo rm -rf " + path_other
                    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
                except:
                    pass
            else:
                print "file: %s have only 1 segment" ,video_name
                logging.info("file: %s have only 1 segment" ,video_name)

        except IOError as exc:
            if exc.errno != errno.EISDIR:  # Do not fail if a directory is found, just ignore it.
                raise  # Propagate other kinds of IOError.

    runningTime = str(datetime.now() - start_time)
    logging.info("finished script in: %s", runningTime)


logname = logger_name + ".log"
logging.basicConfig(filename=logname,
                    filemode='w',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=int(logging_level))

from datetime import datetime
date_of_exec = datetime.today().strftime('%d-%m-%y')

run(path_save_files, date_of_exec)