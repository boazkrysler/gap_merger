import logging
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_images_on_storage =  config.get('paths', 'path_images_on_storage')

class Track:
    def __init__(self, track_id, label, attributes, track_start_segment):
        self.track_id = track_id
        self.label = label
        self.attributes = attributes
        self.track_start_segment = track_start_segment

    def print_track(self):
        print "Track: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " attributes: " , self.attributes
    def get_track(self):
        txt = "Track: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " attributes: " , self.attributes
        return txt


class Line:
    # The class "constructor" - It  's actually an initializer
    def __init__(self, track_id, x_min, y_min, x_max, y_max, frame, lost, occluded, generated, label, attributes, track_start_segment):
        self.track_id = int(track_id)
        self.x_min = int(x_min)
        self.y_min = int(y_min)
        self.x_max = int(x_max)
        self.y_max = int(y_max)
        self.frame = int(frame)
        self.lost = int(lost)
        self.occluded = int(occluded)
        self.generated = int(generated)
        self.label = label
        self.attributes = attributes
        self.track_start_segment = track_start_segment

    def print_line(self):
        print "Line: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " frame:" , self.frame
    def get_line(self):
        line = "Line: track_id: " + str(self.track_id) + " label: " + str(self.label) + " start_segment: " + str(self.track_start_segment) + " frame:" + str(self.frame)
        return line
    def print_full_line(self):
        print "Line: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " frame:" \
            , self.frame


def make_Line(track_id, x_min, y_min, x_max, y_max, frame, lost, occluded, generated, label, attributes, segment):
    return Line(track_id, x_min, y_min, x_max, y_max, frame, lost, occluded, generated, label, attributes, segment)


def getVideoName(name):
    list = name.split('/')
    name = list[-1]
    return name[:-4]


def add_line_to_track_list(line, track_list, track_start_segment):
    if len(track_list) == 0:
        track_list.append(Track(line[0], line[9], line[10:], int(track_start_segment)))
    else:
        for i in range(0, len(track_list)):
            if track_list[i].track_id == line[0]:
                return track_list
        track_list.append(Track(line[0], line[9], line[10:], int(track_start_segment)))
    return track_list


def read_txt_file(name):
    txt_lines = []
    track_list = []
    track_start_segment = 1
    track_id = "-1"

    temp_txt_lines = []
    with open(name) as f:  # No need to specify 'r': this is the default.
        video_name = getVideoName(name)
        logging.info("gap_bb_printer..: opening txt file for reading: %s", str(video_name))
        for line_counter, file_line in enumerate(f):
            line = file_line.split(' ')
            try:
                temp_txt_lines.append(line)
            except:
                print "FAIL"

    for line in temp_txt_lines:
        try:

            if int(line[5]) % 300 == 0 and track_id != line[0]:
                track_start_segment = (int(line[5]) / 300) + 1
                track_id = line[0]

            txt_lines.append(Line(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9],
                               line[10:], int(track_start_segment)))
            track_list = add_line_to_track_list(line, track_list, track_start_segment)
        except:
            logging.debug("error reading line")

    return txt_lines, track_list


def find_max_number_of_frames(txt_lines):
    last_line = txt_lines[:-1]
    return int(last_line[-1].frame)


#not needed anymore
'''
def make_video(path, video_name):

    cmdLine = "cd " + path + " ; ffmpeg -f image2 -r 35 -i %d.jpg -vcodec mpeg4 -y " + video_name
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")

    logging.debug("make_video: video_name: %s", video_name)

    cmdLine = "sudo mv " + path + "/" + video_name + " " + path_save_videos

    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")


def draw_BB_for_all_tracks_on_video(path, txt_lines, main_track_id, track_id_list, video_name, color):
    create_folder(path_images_bb_for_all_video + "/" + video_name)

    logging.info("starting add_BB for track_id_list: " + str(main_track_id))
    for i in range(0, len(txt_lines) - 1):
        if i % 1000 == 0 and i > 0:
            logging.debug("draw_BB_for_track_on_all_video: ran over txt_lines %s to %s" % (i - 1000, i))

        for y in range(0, len(track_id_list)):
            if int(txt_lines[i].track_id) == int(track_id_list[y]):
                if txt_lines[i].lost == "0":
                    add_BB(path, txt_lines[i].frame, main_track_id, txt_lines[i].x_min,
                           txt_lines[i].x_max, txt_lines[i].y_min, txt_lines[i].y_max, color)

    return path


def draw_BB_for_track_on_all_video(txt_lines, track_id_list, video_name, track_list):
    path = path_images_bb_for_all_video + "/" + video_name + "/main_track_id_" + str(track_id_list[0])
    create_folder(path_images_bb_for_all_video + "/" + video_name)

    copy_all_images_from_storage_for_track(path, video_name, track_id_list[0], find_max_number_of_frames(txt_lines))
    logging.info("starting add_BB for track_id_list: " + str(track_id_list))
    for i in range(0, len(txt_lines) - 1):
        if i % 1000 == 0 and i > 0:
            logging.debug("draw_BB_for_track_on_all_video: ran over txt_lines %s to %s" % (i - 1000, i))

        for y in range(0, len(track_id_list)):
            if int(txt_lines[i].track_id) == int(track_id_list[y]):
                color = "RED"
                if track_list[int(track_id_list[y])].track_start_segment % 2 == 0:
                    color = "BLUE"
                add_BB(path, txt_lines[i].frame, track_id_list[y], txt_lines[i].x_min,
                       txt_lines[i].x_max, txt_lines[i].y_min, txt_lines[i].y_max, color)

    make_video(path, video_name + "_main_track_id_" + str(track_id_list[0]) + ".mp4")

def draw_BB_on_gaps(txt_lines, video_name, main_track_id, suspected_track_id, gap_number, number_of_tracks_until_segment_2):
    create_BB_folder(video_name)
    max_frame_num = find_max_number_of_frames(txt_lines)
    for i in range(0, max_frame_num):
        if 300 * gap_number <= i < (300 * gap_number + 22):
            path_save_images = copy_image_from_storage(path_save_image_with_borders, video_name, main_track_id, i)

    chmod_777(path_save_images)
    for i in range(0, len(txt_lines)):
        if  300 * gap_number <= int(txt_lines[i].frame) < (300 * gap_number + 22):
            if txt_lines[i].label == '"PERSON"':
                if txt_lines[i].track_id == str(main_track_id):
                    color = "BLUE"
                elif txt_lines[i].track_id == str(suspected_track_id):
                    color = "RED"
                else:
                    color = "GREEN"


                if not (color == "GREEN" and  int(txt_lines[i].track_id) < number_of_tracks_until_segment_2):
                    add_BB(path_save_images, txt_lines[i].frame, txt_lines[i].track_id, txt_lines[i].x_min, txt_lines[i].x_max,
                           txt_lines[i].y_min, txt_lines[i].y_max, color)
'''

#run(path_turkic_txt_files)
