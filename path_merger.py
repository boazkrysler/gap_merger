import ConfigParser
import logging

from csv_handler import write_merger_file, id_exist_merger_file

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

path_save_files = config.get('paths', 'path_save_files')

DB_NAME = config.get('sql', 'DB_NAME')
DB_USER = config.get('sql', 'DB_USER')
DB_PASS = config.get('sql', 'DB_PASS')
DB_HOST = config.get('sql', 'DB_HOST')


def find_max_number_of_frames(txt_lines):
    last_line = txt_lines[:-1]
    return int(last_line[-1].frame)


number_of_segments = 0
def find_number_of_segments(VIDEO_NAME):
    import MySQLdb
    logging.debug("running sql, video name: " + str(VIDEO_NAME))
    global number_of_segments
    if number_of_segments != 0:
        return number_of_segments
    db = MySQLdb.connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
    cursor = db.cursor()
    cursor.execute("SELECT * FROM videos v WHERE slug='" + str(VIDEO_NAME) + "'")
    data = cursor.fetchone()
    if not data:
        print 'failed load video meta from DB'
        exit(2)
    VIDEO_ID = data[0]

    cursor.execute("SELECT * FROM segments WHERE videoid={}".format(VIDEO_ID))
    data = cursor.fetchall()
    if not data:
        print 'failed load video meta from DB'
        exit(2)
    db.close()
    SEGMENTS = data
    number_of_segments = len(SEGMENTS)
    return number_of_segments

def find_number_of_tracks_until_segment_x(track_list, segment_num):
    count = 0
    if str(segment_num) == "1":
        return 0
    for i in range(0,len(track_list)):
        if int(track_list[i].track_start_segment) < int(segment_num):
            count += 1

    return count

def find_gap_lines_for_segment_y_in_gap_x(txt_lines, segment_num, gap_number, track_list):
    track_id = 0
    num_of_tracks_until_segment = int(find_number_of_tracks_until_segment_x(track_list,segment_num))

    gap_lines = [list] * int(find_number_of_tracks_in_segment(txt_lines,segment_num))
    for i in range(0, len(gap_lines)):
        gap_lines[i] = []

    for i in range(0, len(txt_lines) - 1):
        if int(txt_lines[i].frame) % 300 == 0:
            track_id = txt_lines[i].track_id

        if track_id == txt_lines[i].track_id and int(txt_lines[i].frame) % 300 < 22 and 300 * gap_number <= int(
                txt_lines[i].frame) < 300 * (gap_number + 1):

            track_id_num = int(track_id)
            if int(txt_lines[i].track_start_segment) == int(segment_num) and track_id_num - num_of_tracks_until_segment < len(gap_lines):
                try:
                    gap_lines[track_id_num - num_of_tracks_until_segment].append(txt_lines[i])
                except:
                    logging.debug("Error: track_id_num - num_of_tracks_until_segment =" + str(track_id_num - num_of_tracks_until_segment))

            elif txt_lines[i].track_start_segment > segment_num:
                return gap_lines

    return gap_lines

#Here a,b are bounding boxes in our regular format : col, row, width, height
# col,row = point up left
def rec_intersect(a,b):
    row_intersect = max(0, min(a[0] + a[2], b[0] + b[2]) - max(a[0], b[0]))
    col_intersect = max(0, min(a[1] + a[3], b[1] + b[3]) - max(a[1], b[1]))

    area_intersect = col_intersect * row_intersect
    area_union = a[2]*a[3] + b[2]*b[3] - area_intersect
    try:
        distance = float(area_intersect) / float(area_union)
    except:
        return 0

    return distance

def make_bounding(x1, y1, x2, y2):
    bound = [float]*4
    bound[0] = x1
    bound[1] = y1
    bound[2] = x2 - x1
    bound[3] = y2 - y1
    return bound

def calculate_distance_in_frame_from_track_id_in_gap_1_to_all_tracks_in_gap_2(track_id, frame_num, gap_lines_segment_1, gap_lines_segment_2,
                                                                              number_of_tracks_in_segment_2, number_of_tracks_until_segment_1):
    distance_array = []
    line1 = None

    track_id_num_in_array = int(track_id) - number_of_tracks_until_segment_1
    for y in range(0, len(gap_lines_segment_1[track_id_num_in_array])):
        if str(gap_lines_segment_1[track_id_num_in_array][y].frame) == str(frame_num):
            line1 = gap_lines_segment_1[track_id_num_in_array][y]
            break

    if line1 is None:
        logging.debug("line1 is None:")
        logging.debug( "track_id: " + str(track_id) + " frame num: "  + str(frame_num) + " num_of_tracks_in_seg_2: "  + str(number_of_tracks_in_segment_2))
        return [0] * number_of_tracks_in_segment_2


    for i in range(0, number_of_tracks_in_segment_2):
        distance = 0
        line2 = gap_lines_segment_2[i][0]

        if str(line1.label) == str(line2.label):
            for y in range(0, len(gap_lines_segment_2[i])):

                if str(gap_lines_segment_2[i][y].frame) == str(frame_num):
                    line2 = gap_lines_segment_2[i][y]

                    #check label match:
                    a = make_bounding(int(line1.x_min), int(line1.y_min), int(line1.x_max), int(line1.y_max))
                    b = make_bounding(int(line2.x_min), int(line2.y_min), int(line2.x_max), int(line2.y_max))
                    distance = rec_intersect(a, b)
                    break

        distance_array.append(distance)
    return distance_array


def calculate_distance_matrix_for_track_id_for_all_frames(txt_lines, track_id, gap_lines_segment_1,
                                                          gap_lines_segment_2, number_of_tracks_until_segment_1, segment_2_number):
    gap = int(segment_2_number) - 1
    distance_matrix = []
    for i in range(300 * gap, (300 * gap) + 22):    # i = frame
        distance_matrix.append(calculate_distance_in_frame_from_track_id_in_gap_1_to_all_tracks_in_gap_2(track_id,
                                i, gap_lines_segment_1, gap_lines_segment_2,
                                find_number_of_tracks_in_segment(txt_lines, segment_2_number), number_of_tracks_until_segment_1))
    return distance_matrix

#find distance matrix for 1 gap
def calculate_sum_of_distance(distance_matrix, number_of_tracks_in_segment_2):
    distance_list = [0] * number_of_tracks_in_segment_2
    for i in range(0, number_of_tracks_in_segment_2):
        for y in range(0,len(distance_matrix)):
            distance_list[i] += distance_matrix[y][i]
        distance_list[i] /= 21
    return distance_list


def find_percentage_from_track_id_to_suspected_track_id(distance_matrix, suspected_track_id):
    avg = 0
    best = 0
    worst = 100
    try:
        avg = 0
        best = 0
        worst = 100
        for i in range(0, len(distance_matrix)):
            avg += distance_matrix[i][suspected_track_id]
            if distance_matrix[i][suspected_track_id] > best:
                best = distance_matrix[i][suspected_track_id]
            if distance_matrix[i][suspected_track_id] < worst:
                worst = distance_matrix[i][suspected_track_id]

        avg /= 22
        return "{0:.0f}%".format(best * 100), "{0:.0f}%".format(avg * 100) , "{0:.0f}%".format(worst * 100)
    except:
        logging.warning("error at find_percentage_from_track_id_to_suspected_track_id, id: " + str(suspected_track_id))
        logging.warning("distance matrix: " +str(distance_matrix))
        return "{0:.0f}%".format(best * 100), "{0:.0f}%".format(avg * 100) , "{0:.0f}%".format(worst * 100)



def find_if_track_lost_or_occluded(track_id, txt_lines, segment_1_number):
    lost = False
    occluded = True
    count_lost = 0
    for i in range(0, len(txt_lines)-1):
        if txt_lines[i].track_id == str(track_id):
            if (segment_1_number * 300) + 22 > int(txt_lines[i].frame) >= segment_1_number * 300:
                if txt_lines[i].lost == "1":
                    count_lost += 1
                if txt_lines[i].occluded == "0":
                    occluded = False
    if count_lost > 18:
        lost = True
    return lost, occluded


def merge_multi_pick_track_by_max(list_of_combined_tracks, list_of_distance_list_for_track, number_of_tracks_until_segment_2):
    logging.debug("list_of_combined_tracks before merge_multi_pick_track_by_max: ")
    logging.debug(list_of_combined_tracks)
    logging.debug(list_of_distance_list_for_track)
    for i in range(len(list_of_combined_tracks)):
        if int(list_of_combined_tracks[i][1]) == -3: #multi_pick = -3
            max_distance = 0.5
            #print (list_of_combined_tracks[i])
            for y in range(len(list_of_distance_list_for_track[i])):
                if list_of_distance_list_for_track[i][y] >= max_distance:
                    max_distance = list_of_distance_list_for_track[i][y]
                    track_to_merge = y

            #print "track_to_merge: " + str(track_to_merge)
            max_merge_counter = 0
            for y in range(len(list_of_distance_list_for_track)):
                max_distance = 0.5
                max_distance_track = -1
                for h in range(len(list_of_distance_list_for_track[i])):
                    if list_of_distance_list_for_track[y][h] >= max_distance:
                        max_distance = list_of_distance_list_for_track[y][h]
                        max_distance_track = h
                #print "max_distance_track: " + str(max_distance_track)

                if max_distance_track == track_to_merge:
                    max_merge_counter += 1

            #print "max_merge_counter: %s" %max_merge_counter
            if max_merge_counter == 1:  #merge path
                list_of_combined_tracks[i] = (list_of_combined_tracks[i][0], track_to_merge + number_of_tracks_until_segment_2)
    logging.debug("list_of_combined_tracks after merge_multi_pick_track_by_max: ")
    logging.debug(list_of_combined_tracks)
    return list_of_combined_tracks


def calculate_distance_for_all_frames_in_gap(video_name, txt_lines, track_list, gap_lines_segment_1, gap_lines_segment_2, segment_1_number, segment_2_number):
    number_of_tracks_in_segment_1 = find_number_of_tracks_in_segment(txt_lines, segment_1_number)
    number_of_tracks_in_segment_2 = find_number_of_tracks_in_segment(txt_lines, segment_2_number)

    number_of_tracks_until_segment_1 = find_number_of_tracks_until_segment_x(track_list, segment_1_number)
    number_of_tracks_until_segment_2 = find_number_of_tracks_until_segment_x(track_list, segment_2_number)
    match_counter = 0
    multi_match_counter = 0

    list_of_combined_tracks = []
    list_of_distance_list_for_track = []
    assigned_set = set([])
    lost_counter = 0
    person_label_counter = 0
    merge_multi_pick = False
    #print "segment number: " + str(segment_1_number)
    for i in range(0, number_of_tracks_in_segment_1):
        track_id = i + number_of_tracks_until_segment_1

        if track_id not in already_merged_tack_list:
            distance_matrix = calculate_distance_matrix_for_track_id_for_all_frames(txt_lines, track_id, gap_lines_segment_1, gap_lines_segment_2,
                                                                                    number_of_tracks_until_segment_1, segment_2_number)
            # not needed anymore
            #write_distance_list_for_track_chart(distance_matrix, video_name, track_id, gap_number = segment_1_number, merge_part = 1,
             #                                   number_of_tracks_until_segment_2 = number_of_tracks_until_segment_2)

            distance_list_for_track = calculate_sum_of_distance(distance_matrix, number_of_tracks_in_segment_2)
            logging.debug("track_id %s  distance_list: %s" %(track_id, distance_list_for_track))
            list_of_distance_list_for_track.append(distance_list_for_track)
            distance, suspected_track_id, counter_tracks_above_rate = find_optimal_distance(distance_list_for_track, number_of_tracks_until_segment_2, merge_rate=0.5)

            best_percentage, avg_percentage, worst_percentage = find_percentage_from_track_id_to_suspected_track_id(distance_matrix, suspected_track_id - number_of_tracks_until_segment_2)
            lost, occluded = find_if_track_lost_or_occluded(track_id, txt_lines, segment_1_number)


            label = track_list[int(track_id)].label.translate(None, '"')
            label = label.translate(None, "'")

            #not needed any more
            #write_video_report_summery(video_name, segment_1_number, label, track_id, suspected_track_id,
             #                              best_percentage, avg_percentage, worst_percentage, lost, occluded, date, counter_tracks_above_rate)


            if distance > 0.5 and counter_tracks_above_rate == 1 and not assigned_set.__contains__(suspected_track_id):
                match_counter += 1
            if counter_tracks_above_rate > 1 or assigned_set.__contains__(suspected_track_id):
                multi_match_counter += 1

            if distance > 0.5 and counter_tracks_above_rate == 1 and not assigned_set.__contains__(suspected_track_id):
                node = (track_id, suspected_track_id)
            elif counter_tracks_above_rate > 1:
                node = (track_id, -3)   # multi-pick
                merge_multi_pick = True
                logging.debug("multi_pick: " + str(distance_list_for_track))
            else:
                node = (track_id, -50)   # distance < 0.5
                if lost:
                    lost_counter += 1

            list_of_combined_tracks.append(node)
            #line = "track id = %s label = %s best_distance = %s multi_merge = %s"  % (track_id, label, distance, counter_tracks_above_rate)
            #logging.debug(line)

            #only for prints
            if counter_tracks_above_rate > 1:
                logging.debug("distance_list_for_track[%s]: %s" %( track_id, distance_list_for_track))
                logging.debug("track we try to merge seg1: " +str(track_list[track_id].get_track()))
                for h in range(number_of_tracks_until_segment_2, number_of_tracks_until_segment_2 + number_of_tracks_in_segment_2):
                    if distance_list_for_track[h - number_of_tracks_until_segment_2] > 0.5:
                        logging.debug("track we try to merge seg2: " + str(track_list[h].get_track()))
            #------

            assigned_set.add(suspected_track_id)

            #merge_success report file:
            if label == "PERSON":
                person_label_counter += 1

        else:   # track_id in already_merged_tack_list:
            for h in range(len(already_merged_tack_list)):
                if already_merged_tack_list[h] == track_id:
                    if track_list[track_id].track_start_segment < track_list[already_merged_tack_list[h+1]].track_start_segment:
                        node = (track_id, already_merged_tack_list[h+1])
                        list_of_combined_tracks.append(node)
                        break

                    #-------------------------------    end track loop -------------------------------------------------------------------

    ## until here was the first stage of the merge. by % > 0.5 only
    # second stage: combine if destined track_id have only 1 segment_1 track id that consider him as his best distance

    if merge_multi_pick:
        list_of_combined_tracks = merge_multi_pick_track_by_max(list_of_combined_tracks, list_of_distance_list_for_track, number_of_tracks_until_segment_2)

    for i in range(len(list_of_combined_tracks)):
        if list_of_combined_tracks[i][1] >= 0:
            already_merged_tack_list.append(list_of_combined_tracks[i][0])

    # not needed anymore
    #write_merge_success_report(video_name, list_of_combined_tracks , segment_1_number, number_of_tracks_in_segment_1 ,lost_counter)

    line = ("number of trucks in segment 1: %s , in segment 2: %s , number of matches: %s , number of multi matchs: %s"  % (number_of_tracks_in_segment_1,
                number_of_tracks_in_segment_2, match_counter, multi_match_counter))
    logging.info(line)
    return list_of_combined_tracks

global date
date = ""


def find_next_element_of_node_start_in_id(list_of_combined_tracks, start_id):
    for i in range(len(list_of_combined_tracks)):
        for y in range(len(list_of_combined_tracks[i])):
            if list_of_combined_tracks[i][y][0] == start_id:
                return list_of_combined_tracks[i][y][1]
    return -1


global already_merged_tack_list
def find_similar_paths(txt_lines, video_name, track_list, date_of_exec, merged_list, print_video):
    global date, already_merged_tack_list
    already_merged_tack_list = merged_list

    color_list_1 = ["GREEN", "RED", "BLUE", "BLACK" ,"ORANGE", "WHITE", "PURPLE"]
    color_list_2 = ["BROWN", "TEAL", "PINK", "GREY"]
    color_list_counter = 0
    date = date_of_exec
    list_of_combined_tracks = []
    logging.info("starting find_similar_paths for video: %s", video_name)

    number_of_segments = find_number_of_segments(video_name)
    for i in range(1, number_of_segments):
        logging.info("gap number: %s" % i)
        gap_lines_segment_1 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, i, i, track_list)
        gap_lines_segment_2 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, i + 1, i, track_list)
        list_of_combined_tracks.append(calculate_distance_for_all_frames_in_gap(video_name, txt_lines, track_list, gap_lines_segment_1,gap_lines_segment_2, i, i+1))

    # draw bb on track all video
        #build track path id list
    last_track_id = int(track_list[-1].track_id)
    assigned_set = set([])
    path_person = " "
    path_others = " "
    for i in range(last_track_id):
        if not assigned_set.__contains__(i):
            list_of_id_for_track_path = []
            for y in range(1 , number_of_segments):
                if track_list[i].track_start_segment > y:
                    list_of_id_for_track_path.append("-5")
            first_real_track_id = i
            list_of_id_for_track_path.append(first_real_track_id)
            next_track_id = i
            while next_track_id > -1:
                next_track_id = find_next_element_of_node_start_in_id(list_of_combined_tracks, start_id = next_track_id)
                list_of_id_for_track_path.append(next_track_id)
                assigned_set.add(next_track_id)

            line = "list_of_id_for_track_path for track (%s): %s" % ( i, list_of_id_for_track_path)
            logging.info(line)


            color = color_list_1[color_list_counter % len(color_list_1)]
            if int(track_list[first_real_track_id].track_start_segment) % 2 == 1:
                color = color_list_2[color_list_counter % len(color_list_2)]

            color_list_counter += 1
            count_real_merge = 0
            for id_in_list in list_of_id_for_track_path:
                if int(id_in_list) >= 0:
                    count_real_merge += 1

            #if len(list_of_id_for_track_path) > 2 and count_real_merge > 1:
            if len(list_of_id_for_track_path) > 2 and not id_exist_merger_file(video_name,list_of_id_for_track_path) and count_real_merge > 1:
                write_merger_file(video_name, list_of_id_for_track_path[:-1])

    return path_person, path_others

def find_optimal_distance(distance_list, number_of_tracks_until_second_segment, merge_rate):
    track_id = -1
    distance = 0
    counter = 0
    for i in range(0, len(distance_list)):
        if distance_list[i] > distance:
            distance = distance_list[i]
            track_id = i

        if distance_list[i] > merge_rate:
            counter += 1

    track_id = track_id + number_of_tracks_until_second_segment
    return distance, track_id, counter


def find_number_of_tracks_in_segment(txt_lines, segment_num):
    count = 0
    track_id = "-1"
    for i in range(0, len(txt_lines)):
        if not txt_lines[i].track_id == track_id:
            track_id = txt_lines[i].track_id
            if int(txt_lines[i].track_start_segment) == segment_num:
                count += 1
    return count



'''
#not needed anymore
def write_merge_success_report(video_name, list_of_combined_tracks, gap_number, tracks_in_segment ,lost_counter):

    num_of_merges = 0
    for i in range(len(list_of_combined_tracks)):
        if list_of_combined_tracks[i][1] >= 0 and isinstance(list_of_combined_tracks[i][1], int) :
            num_of_merges += 1

    write_merge_success_report_to_csv(video_name, gap_number, tracks_in_segment ,lost_counter , num_of_merges, date)
'''
