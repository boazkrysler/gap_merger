import glob
import logging
import ConfigParser

import errno

import time
import subprocess

import sys

from gap_BB_printer import read_txt_file
from csv_handler import create_report_folder, create_merger_file, get_all_id_from_merger_file
from path_merger import find_similar_paths, find_number_of_segments

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
logging_level = config.get('logging', 'logging_level')
logger_name = config.get('logging', 'name')

path_save_files = config.get('paths', 'path_save_files')

def turkic_dump_command(video_name):

    cmdLine = "cd /root/vatic ; sudo turkic dump " + video_name + " -o " + path_save_files + "/" + video_name + "/vatic_dump.txt"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    logging.debug("turkicDumpCommand: cmdLine: %s", cmdLine)

def run(video_name, new_merger_file, date_of_exec):
    logging.info("video name: %s " %video_name)

    start_time = datetime.now()
    create_report_folder(video_name)
    time.sleep(1)

    turkic_dump_command(video_name)
    time.sleep(5)
    try:
        file_start_time = datetime.now()
        txt_lines, track_list = read_txt_file(path_save_files + "/" + video_name + "/vatic_dump.txt")
        number_of_segments = find_number_of_segments(video_name)

        if number_of_segments > 1:

            logging.info("working on file: %s with %s segments, len(txt_lines) = %s" %(video_name, number_of_segments, len(txt_lines)))
            print "working on file: %s with %s segments" %(video_name, number_of_segments)

            ##merger_file
            print "new_merger_file: " ,new_merger_file
            if new_merger_file:
                merged_list = []
                create_merger_file(video_name, number_of_segments)
            else:
                merged_list = get_all_id_from_merger_file(video_name)

            path_person, path_other = find_similar_paths(txt_lines, video_name, track_list, date_of_exec, merged_list, print_video = False)

            runningTime = str(datetime.now() - file_start_time)
            logging.info("finished file in: %s seconds" % runningTime)

            try:  # delete temp files
                cmdLine = "sudo rm -rf " + path_person
                subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
                cmdLine = "sudo rm -rf " + path_other
                subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
            except:
                pass
        else:
            print "file: %s have only 1 segment" %video_name
            logging.info("file: %s have only 1 segment" %video_name)

    except IOError as exc:
        if exc.errno != errno.EISDIR:  # Do not fail if a directory is found, just ignore it.
            raise  # Propagate other kinds of IOError.

    runningTime = str(datetime.now() - start_time)
    logging.info("finished script in: %s", runningTime)


logname = logger_name + ".log"
logging.basicConfig(filename=logname,
                    filemode='w',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=int(logging_level))

from datetime import datetime
date_of_exec = datetime.today().strftime('%d-%m-%y')

if len(sys.argv) < 2:
    print "please enter video name, you may also add '0' for new merger file"
    exit()
try:
    video_name = sys.argv[1]
except:
    exit()

new_merger_file = True
if len(sys.argv) > 2:
    new_merger_file = sys.argv[2]
    if new_merger_file == "0":
        new_merger_file = False

run(video_name, new_merger_file, date_of_exec )