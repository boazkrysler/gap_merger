import csv
import os
import subprocess

import ConfigParser


config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

path_save_files = config.get('paths', 'path_save_files')

def create_folder(path):
    if not os.path.exists(path):
        subprocess.call(['sudo', 'mkdir', path])

def create_report_folder(video_name):
    files_path = path_save_files + "/" + video_name
    create_folder(files_path)
    cmdLine = "sudo chmod -R 777 " + files_path + "/"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)


def create_merger_file(video_name, number_of_segments):

    my_file = path_save_files + "/" + video_name + "/pre_merge.csv"

    segment_list = []
    for i in range(number_of_segments):
        segment_list.append('segment_' + str(i + 1))

    with open(my_file, 'w') as csv_file:

        csvWriter = csv.DictWriter(csv_file, fieldnames=segment_list)
        csvWriter.writeheader()

        cmdLine = "sudo chmod -R 777 " + my_file
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
        return True
    return False

def id_exist_merger_file(video_name, list_track_id):

    my_file = path_save_files + "/" + video_name + "/pre_merge.csv"
    with open(my_file) as infile:
        r = csv.DictReader(infile, skipinitialspace=True)
        for row in r:
            for val in row.itervalues():
		if str(val) != "-5" and str(val) != "-8":
			for i in range(len(list_track_id)):
		                if str(val) == str(list_track_id[i]):
        		            return True
    return False


def write_merger_file(video_name, track_id_list ):

    my_file = path_save_files + "/" + video_name + "/pre_merge.csv"
    with open(my_file, 'a') as my_file:

        csvWriter = csv.writer(my_file, delimiter=',')
        csvWriter.writerow(track_id_list)

    return True

def get_all_id_from_merger_file(video_name):

    my_file = path_save_files + "/" + video_name + "/pre_merge.csv"

    track_id_list = []
    with open(my_file, 'r') as my_file:

        readCSV = csv.reader(my_file, delimiter=',')
        for row in readCSV:
            if len(row) > 1:
                for i in range(len(row)):
                    if row[i].isdigit():
                        track_id_list.append(int(row[i]))

    return track_id_list

#Satistcs about merge ratio and success rate:
#not needed any more
'''
def write_video_report_summery(video_name, gap_number, track_label, main_track_id, suspected_track_id, best_percentage, avg_percentage,
                               worst_percentage, lost, occluded, date, counter_tracks_above_rate):

    file_name = "merger_report_summery_" + str(video_name) + "_date_" + str(date) + ".csv"
    my_file = path_save_csv + "/" + video_name + "/" + file_name

    fieldnames = ['Video_Name', 'gap_number', 'main_track_id', 'track_label',
                  'suspected_track_id', 'best_percentage', 'avg_percentage', 'worst_percentage', 'main_track_lost', 'main_track_occluded', 'number_of_suspected_tracks']
    if not os.path.isfile(my_file):
        with open(my_file, 'w') as csv_file:

            csvWriter = csv.DictWriter(csv_file, fieldnames=fieldnames)
            csvWriter.writeheader()

            cmdLine = "sudo chmod -R 777 " + my_file
            subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    with open(my_file, 'a') as csv_file:

        csvWriter = csv.DictWriter(csv_file, fieldnames=fieldnames)
        csvWriter.writerow(
            {'Video_Name': video_name, 'gap_number': gap_number, 'main_track_id': main_track_id, 'track_label': track_label,
                  'suspected_track_id': suspected_track_id, 'best_percentage': best_percentage,
             'avg_percentage': avg_percentage, 'worst_percentage': worst_percentage, 'main_track_lost': lost, 'main_track_occluded': occluded,
             'number_of_suspected_tracks': counter_tracks_above_rate})

#not needed anymore
def write_merge_success_report_to_csv(video_name, gap_number, num_of_trucks_in_gap ,lost_counter, num_of_merges, date):

    file_name = "merge_success_report_date_" + str(date) + ".csv"
    my_file = path_save_csv + "/" + file_name

    fieldnames = ['video', 'gap_number', 'suspected_possible_merges','num_of_merges', '%success', 'num_of_trucks_in_gap' ,'lost_counter']
    if not os.path.isfile(my_file):
        with open(my_file, 'w') as csv_file:

            csvWriter = csv.DictWriter(csv_file, fieldnames=fieldnames)
            csvWriter.writeheader()

            cmdLine = "sudo chmod -R 777 " + my_file
            subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

    suspected_possible_merges = num_of_trucks_in_gap - lost_counter
    with open(my_file, 'a') as csv_file:
        success = "{0:.0f}%".format(0)
        if suspected_possible_merges != 0:
            success = float(float(num_of_merges) / float(suspected_possible_merges))
            success = "{0:.0f}%".format(success * 100)
        csvWriter = csv.DictWriter(csv_file, fieldnames=fieldnames)
        csvWriter.writerow(
            {'video': video_name, 'gap_number': gap_number, 'suspected_possible_merges': suspected_possible_merges,
             'num_of_merges': num_of_merges, '%success': success, 'num_of_trucks_in_gap':num_of_trucks_in_gap ,'lost_counter': lost_counter})


#not needed anymore
def write_distance_list_for_track_chart(distance_matrix, video_name, track_id, gap_number, merge_part, number_of_tracks_until_segment_2):
    file_name = "distance_matrix_merge_part_" + str(merge_part) + "_track_id_" + str(track_id) + "_gap_number_" + str(gap_number) + ".csv"
    my_file = path_save_csv + "/" + video_name + "/" + file_name
    number_of_columns = len(distance_matrix[0])
    with open(my_file, 'w') as csv_file:

        cmdLine = "sudo chmod -R 777 " + my_file
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        wr = csv.writer(csv_file, dialect='excel')
        lst = range(number_of_tracks_until_segment_2, number_of_tracks_until_segment_2 + number_of_columns)

        wr.writerow(["track_id/frame_number"] + lst)

        for i in range(0, len(distance_matrix)):
            print_list = []
            for y in range(0,len(distance_matrix[i])):
                print_list.append("{0:.0f}%".format(distance_matrix[i][y] * 100))

            wr.writerow([i] + print_list)
'''
