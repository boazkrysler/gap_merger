import unittest

from gap_BB_printer import read_txt_file, draw_BB_on_gaps
from path_merger import find_gap_lines_for_segment_y_in_gap_x, find_number_of_tracks_in_segment, calculate_distance_in_frame_from_track_id_in_gap_1_to_all_tracks_in_gap_2, find_optimal_distance, \
    find_number_of_tracks_until_segment_x, calculate_distance_for_all_frames_in_gap, \
    calculate_distance_matrix_for_track_id_for_all_frames, merge_multi_pick_track_by_max
from csv_handler import create_report_folder
import ConfigParser

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_turkic_txt_files =  config.get('paths', 'path_turkic_txt_files')


class TestUM(unittest.TestCase):

    def setUp(self):
        path = path_turkic_txt_files + "/"
        txt_lines, video_name, track_list = read_txt_file(path)
        create_report_folder(video_name)
    '''
    def test_gap_len(self):
        path = path_turkic_txt_files + "/"
        txt_lines, video_name, track_list = read_txt_file(path)
        gap_lines = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 2, 1, track_list)

        number_of_tracks_in_segment = find_number_of_tracks_in_segment(txt_lines, segment_num=1)
        gap_len_user = number_of_tracks_in_segment * 22
        test_gap_len = 0
        for i in range(0,len(gap_lines)):
            test_gap_len += len(gap_lines[i])

        self.assertEqual(gap_len_user, test_gap_len)

    def test_find_optimal_distance(self):
        distance, track_id = find_optimal_distance([0.5, 0.9, 0.3, 0.23423423, 0.15], 0)
        self.assertEqual(distance, 0.9)
        self.assertEqual(track_id, 1)


    def test_calculate_distance_in_frame_from_track_id_in_gap_1_to_all_tracks_in_gap_2(self):
        path = path_turkic_txt_files + "/"
        txt_lines, video_name, track_list = read_txt_file(path)
        gap_lines_segment_1 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 1, 1, track_list)
        gap_lines_segment_2 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 2, 1, track_list)

        distance_list = calculate_distance_in_frame_from_track_id_in_gap_1_to_all_tracks_in_gap_2(2,
                            300, gap_lines_segment_1, gap_lines_segment_2, find_number_of_tracks_in_segment(txt_lines,2),
                                                            number_of_tracks_until_segment_1 = 0)

        num_of_tracks_until_segment_2 = int(find_number_of_tracks_until_segment_x(track_list, 2))
        distance, track_id = find_optimal_distance(distance_list, num_of_tracks_until_segment_2)

        self.assertEqual(9, track_id)


    def test_calculate_distance_matrix_for_track_for_all_frames(self):
        path = path_turkic_txt_files + "/"
        txt_lines, video_name, track_list = read_txt_file(path)

        gap_lines_segment_1 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 1, 1, track_list)
        gap_lines_segment_2 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 2, 1, track_list)

        number_of_tracks_until_segment_1 = find_number_of_tracks_until_segment_x(track_list, 1)

        distance_matrix = calculate_distance_matrix_for_track_id_for_all_frames(txt_lines, 2, gap_lines_segment_1,
                                                                                gap_lines_segment_2, number_of_tracks_until_segment_1, segment_2_number=2)

        print "test_calculate_distance_matrix_for_frame: distance matrix:"
        for i in range(0, len(distance_matrix)):
            print distance_matrix[i]


    def test_calculate_distance_for_all_frames_in_gap(self):
        path = path_turkic_txt_files + "/"
        txt_lines, video_name, track_list = read_txt_file(path)
        gap_lines_segment_1 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 1, 1, track_list)
        gap_lines_segment_2 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 2, 1, track_list)

        distance_matrix_for_all_frames = calculate_distance_for_all_frames_in_gap(video_name, txt_lines, track_list, gap_lines_segment_1, gap_lines_segment_2, 1, 2)
        ##print "distance_for_track_on_all_frames:"
        ##for i in range(0,len(distance_matrix_for_all_frames)):
          ##  print distance_matrix_for_all_frames[i]


    def test_print_bb(self):
        path = path_turkic_txt_files + "/"
        txt_lines, video_name, track_list = read_txt_file(path)
        gap_lines_segment_1 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 1, 1, track_list)
        gap_lines_segment_2 = find_gap_lines_for_segment_y_in_gap_x(txt_lines, 2, 1, track_list)

        distance_list = calculate_distance_in_frame_from_track_id_in_gap_1_to_all_tracks_in_gap_2(2,
                            300, gap_lines_segment_1, gap_lines_segment_2, find_number_of_tracks_in_segment(txt_lines,2),
                                                            number_of_tracks_until_segment_1 = 0)
        number_of_tracks_until_segment_2 = find_number_of_tracks_until_segment_x(track_list,2)
        print distance_list

        draw_BB_on_gaps(txt_lines,video_name,2,11, 1, number_of_tracks_until_segment_2)
    '''
    def test_merge_multi_pick_track_by_max_dont_change(self):
        print "test1"
        list_node = []
        number_of_tracks_until_segment_2 = 4
        node = (1,"multi_pick")
        list_node.append(node)
        node = (2,"multi_pick")
        list_node.append(node)
        node = (3,6)
        list_node.append(node)

        list_of_distance_list_for_track = [[0.6, 0.9, 0], [0.7, 0.8, 0], [0.2, 0.3, 0.6]]

        new_list_of_combined_tracks = list(merge_multi_pick_track_by_max(list_node, list_of_distance_list_for_track,
                                          number_of_tracks_until_segment_2))

        self.assertListEqual(new_list_of_combined_tracks, list_node)

    def test_merge_multi_pick_track_by_max_change(self):
        print "test2"

        list_node = []
        number_of_tracks_until_segment_2 = 4
        node = (1,"multi_pick")
        list_node.append(node)
        node = (2,"multi_pick")
        list_node.append(node)
        node = (3,6)
        list_node.append(node)

        list_of_distance_list_for_track = [[0.6,0.9,0],[0.9,0.6,0],[0.2,0.3,0.6]]
        new_list_of_combined_tracks = list(merge_multi_pick_track_by_max(list_node, list_of_distance_list_for_track,
                                          number_of_tracks_until_segment_2))
        self.assertListEqual(new_list_of_combined_tracks, list_node)
        print new_list_of_combined_tracks


if __name__ == '__main__':
    unittest.main()

